# Docker Image for Arch Package Builds

This Docker image contains `base`, `base-devel` and `git`.

A user `build` is added and has `sudo` permission without password.

Furthermore, the `multilib` repository is enabled.
