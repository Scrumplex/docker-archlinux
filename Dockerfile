FROM archlinux:base
MAINTAINER Sefa Eyeoglu <contact@scrumplex.net>

# Default configuration
COPY pacman-stock.conf /etc/pacman.conf
COPY makepkg.conf /etc/makepkg.conf

RUN pacman -Syu --needed --noconfirm --noprogressbar base base-devel git python3 npm node-gyp yarn cmake check && pacman -Sc --noconfirm --noprogressbar

RUN useradd -m -U build && echo "build ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers

# Final
USER build
WORKDIR /home/build
